# frozen_string_literal: true

class ApplicationController < ActionController::Base

	before_action :configure_permitted_parameters, if: :devise_controller?
	before_action :set_areas, unless: :devise_controller?

	protected

	def configure_permitted_parameters
		devise_parameter_sanitizer.permit(:sign_up, keys: [:first_name, :last_name])
		# devise_parameter_sanitizer.permit(:account_update, keys: [:first_name, :last_name, :phone, :email, bank_attributes: [:bank_name, :bank_account]])
	end

	def set_areas
		@areas = Area.all
	end

	def at_mobile?
		["iPhone", "Mobile"].any? do |entry|
			request.env["HTTP_USER_AGENT"].include? entry
		end
	end

end
